## `msgid`s in this file come from POT (.pot) files.
##
## Do not add, change, or remove `msgid`s manually here as
## they're tied to the ones in the corresponding POT file
## (with the same domain).
##
## Use `mix gettext.extract --merge` or `mix gettext.merge`
## to merge POT files into PO files.
msgid ""
msgstr ""
"PO-Revision-Date: 2021-05-17 16:22+0000\n"
"Last-Translator: Leo Durruti <leodurruti@disroot.org>\n"
"Language-Team: Italian <https://weblate.framasoft.org/projects/mobilizon/"
"backend-errors/it/>\n"
"Language: it\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.6\n"

#: lib/mobilizon/discussions/discussion.ex:69
msgid "can't be blank"
msgstr "non può essere vuoto"

msgid "has already been taken"
msgstr "e' già stato usato"

msgid "is invalid"
msgstr "è invalido"

msgid "must be accepted"
msgstr "deve esssere accettato"

msgid "has invalid format"
msgstr "ha un formato non valido"

msgid "has an invalid entry"
msgstr "ha un occorrenza non valida"

msgid "is reserved"
msgstr "è riservato"

msgid "does not match confirmation"
msgstr "non corrisponde alla conferma"

msgid "is still associated with this entry"
msgstr "è già associato a questo inserimento"

msgid "are still associated with this entry"
msgstr "sono ancora associati/e a questa voce"

msgid "should be %{count} character(s)"
msgid_plural "should be %{count} character(s)"
msgstr[0] "dovrebbe avere %{count} carattere"
msgstr[1] "dovrebbe avere %{count} caratteri"

msgid "should have %{count} item(s)"
msgid_plural "should have %{count} item(s)"
msgstr[0] "dovrebbe avere %{count} voce"
msgstr[1] "dovrebbe avere %{count} voci"

msgid "should be at least %{count} character(s)"
msgid_plural "should be at least %{count} character(s)"
msgstr[0] "dovrebbe essere di almeno %{count} carattere(i)"
msgstr[1] "dovrebbero essere di almeno %{count} carattere(i)"

msgid "should have at least %{count} item(s)"
msgid_plural "should have at least %{count} item(s)"
msgstr[0] "dovrebbe avere almeno %{count} elemento(i)"
msgstr[1] "dovrebbero avere almeno %{count} elemento(i)"

msgid "should be at most %{count} character(s)"
msgid_plural "should be at most %{count} character(s)"
msgstr[0] "dovrebbe essere di al massimo %{count} carattere(i)"
msgstr[1] "dovrebbero essere di al massimo %{count} carattere(i)"

msgid "should have at most %{count} item(s)"
msgid_plural "should have at most %{count} item(s)"
msgstr[0] "dovrebbe avere al massimo %{count} elemento(i)"
msgstr[1] "dovrebbero avere al massimo %{count} elemento(i)"

msgid "must be less than %{number}"
msgstr "deve essere minore di %{number}"

msgid "must be greater than %{number}"
msgstr "deve essere maggiore di %{number}"

msgid "must be less than or equal to %{number}"
msgstr "dev'essere minore o uguale di %{number}"

msgid "must be greater than or equal to %{number}"
msgstr "dev'essere maggiore o uguale di %{number}"

msgid "must be equal to %{number}"
msgstr "dev'essere uguale a %{number}"

#, elixir-format
#: lib/graphql/resolvers/user.ex:107
msgid "Cannot refresh the token"
msgstr "Il token non può essere aggiornato"

#, elixir-format
#: lib/graphql/resolvers/group.ex:245
msgid "Current profile is not a member of this group"
msgstr "Il profilo corrente non è membro di questo gruppo"

#, elixir-format
#: lib/graphql/resolvers/group.ex:249
msgid "Current profile is not an administrator of the selected group"
msgstr "Il profilo corrente non è amministratore del gruppo selezionato"

#, elixir-format
#: lib/graphql/resolvers/user.ex:592
msgid "Error while saving user settings"
msgstr "Errore nel salvare le preferenze utente"

#, elixir-format
#: lib/graphql/error.ex:99 lib/graphql/resolvers/group.ex:242
#: lib/graphql/resolvers/group.ex:274 lib/graphql/resolvers/group.ex:311 lib/graphql/resolvers/member.ex:79
msgid "Group not found"
msgstr "Gruppo non trovato"

#, elixir-format
#: lib/graphql/resolvers/group.ex:78 lib/graphql/resolvers/group.ex:82
msgid "Group with ID %{id} not found"
msgstr "Gruppo con ID %{id} non trovato"

#, elixir-format
#: lib/graphql/resolvers/user.ex:85
msgid "Impossible to authenticate, either your email or password are invalid."
msgstr "Impossibile autenticarsi: email e/o password non validi."

#, elixir-format
#: lib/graphql/resolvers/group.ex:308
msgid "Member not found"
msgstr "Membro non trovato"

#, elixir-format
#: lib/graphql/resolvers/actor.ex:94
msgid "No profile found for the moderator user"
msgstr "Nessun profilo trovato per l'utente moderatore"

#, elixir-format
#: lib/graphql/resolvers/user.ex:253
msgid "No user to validate with this email was found"
msgstr "Nessun utente da convalidare trovato con questa email"

#, elixir-format
#: lib/graphql/resolvers/person.ex:314 lib/graphql/resolvers/user.ex:278
msgid "No user with this email was found"
msgstr "Nessun utente con questa email"

#, elixir-format
#: lib/graphql/resolvers/feed_token.ex:28
#: lib/graphql/resolvers/participant.ex:32 lib/graphql/resolvers/participant.ex:210 lib/graphql/resolvers/person.ex:236
#: lib/graphql/resolvers/person.ex:353 lib/graphql/resolvers/person.ex:380 lib/graphql/resolvers/person.ex:397
msgid "Profile is not owned by authenticated user"
msgstr "L'utente autenticato non è propietario di questo profilo"

#, elixir-format
#: lib/graphql/resolvers/user.ex:156
msgid "Registrations are not open"
msgstr "Le registrazioni non sono aperte"

#, elixir-format
#: lib/graphql/resolvers/user.ex:407
msgid "The current password is invalid"
msgstr "la password corrente non è valida"

#, elixir-format
#: lib/graphql/resolvers/user.ex:450
msgid "The new email doesn't seem to be valid"
msgstr "La nuova email sembra non valida"

#, elixir-format
#: lib/graphql/resolvers/user.ex:453
msgid "The new email must be different"
msgstr "La nuova email dev'essere diversa"

#, elixir-format
#: lib/graphql/resolvers/user.ex:410
msgid "The new password must be different"
msgstr "La nuova password deve essere diversa"

#, elixir-format
#: lib/graphql/resolvers/user.ex:457 lib/graphql/resolvers/user.ex:519
#: lib/graphql/resolvers/user.ex:522
msgid "The password provided is invalid"
msgstr "La password assegnata non è valida"

#, elixir-format
#: lib/graphql/resolvers/user.ex:414
msgid "The password you have chosen is too short. Please make sure your password contains at least 6 characters."
msgstr "la password scelta è troppo corta, deve avere almeno 6 caratteri."

#, elixir-format
#: lib/graphql/resolvers/user.ex:274
msgid "This user can't reset their password"
msgstr "Questo utente non può resettare la password"

#, elixir-format
#: lib/graphql/resolvers/user.ex:81
msgid "This user has been disabled"
msgstr "L'utente è stato disabilitato"

#, elixir-format
#: lib/graphql/resolvers/user.ex:232 lib/graphql/resolvers/user.ex:237
msgid "Unable to validate user"
msgstr "Impossibile convalidare l'utente"

#, elixir-format
#: lib/graphql/resolvers/user.ex:500
msgid "User already disabled"
msgstr "Utente già disabilitato"

#, elixir-format
#: lib/graphql/resolvers/user.ex:567
msgid "User requested is not logged-in"
msgstr "L'utente richiesto non è loggato"

#, elixir-format
#: lib/graphql/resolvers/group.ex:280
msgid "You are already a member of this group"
msgstr "Sei già un membro di questo gruppo"

#, elixir-format
#: lib/graphql/resolvers/group.ex:315
msgid "You can't leave this group because you are the only administrator"
msgstr "Non puoi lasciare questo gruppo perchè sei l'unico amministratore"

#, elixir-format
#: lib/graphql/resolvers/group.ex:277
msgid "You cannot join this group"
msgstr "Non puoi unirti a questo gruppo"

#, elixir-format
#: lib/graphql/resolvers/group.ex:112
msgid "You may not list groups unless moderator."
msgstr "Non è possibile elencare i gruppi a meno che non sia un moderatore."

#, elixir-format
#: lib/graphql/resolvers/user.ex:465
msgid "You need to be logged-in to change your email"
msgstr "È necessario effettuare il login per modificare la tua email"

#, elixir-format
#: lib/graphql/resolvers/user.ex:422
msgid "You need to be logged-in to change your password"
msgstr "È necessario effettuare il login per modificare la tua password"

#, elixir-format
#: lib/graphql/resolvers/group.ex:254
msgid "You need to be logged-in to delete a group"
msgstr "È necessario effettuare il login per eliminare un gruppo"

#, elixir-format
#: lib/graphql/resolvers/user.ex:527
msgid "You need to be logged-in to delete your account"
msgstr "È necessario effettuare il login per eliminare il tuo account"

#, elixir-format
#: lib/graphql/resolvers/group.ex:285
msgid "You need to be logged-in to join a group"
msgstr "È necessario effettuare il login per entrare a far parte di un gruppo"

#, elixir-format
#: lib/graphql/resolvers/group.ex:320
msgid "You need to be logged-in to leave a group"
msgstr "È necessario effettuare il login per lasciare un gruppo"

#, elixir-format
#: lib/graphql/resolvers/group.ex:218
msgid "You need to be logged-in to update a group"
msgstr "È necessario effettuare il login per aggiornare un gruppo"

#, elixir-format
#: lib/graphql/resolvers/user.ex:112
msgid "You need to have an existing token to get a refresh token"
msgstr ""
"È necessario disporre di un token esistente per ottenere un token di "
"aggiornamento"

#, elixir-format
#: lib/graphql/resolvers/user.ex:256 lib/graphql/resolvers/user.ex:281
msgid "You requested again a confirmation email too soon"
msgstr "Hai richiesto di nuovo un'e-mail di conferma troppo presto"

#, elixir-format
#: lib/graphql/resolvers/user.ex:159
msgid "Your email is not on the allowlist"
msgstr "La tua mail non è nella lista delle autorizzazioni"

#, elixir-format
#: lib/graphql/resolvers/actor.ex:100
msgid "Error while performing background task"
msgstr "Errore nell'eseguire un processo in background"

#, elixir-format
#: lib/graphql/resolvers/actor.ex:32
msgid "No profile found with this ID"
msgstr "Nessun profilo trovato con questo ID"

#, elixir-format
#: lib/graphql/resolvers/actor.ex:61 lib/graphql/resolvers/actor.ex:97
msgid "No remote profile found with this ID"
msgstr "Nessun profilo remoto trovato con questo ID"

#, elixir-format
#: lib/graphql/resolvers/actor.ex:72
msgid "Only moderators and administrators can suspend a profile"
msgstr "Solo i moderatori e gli amministratori possono sospendere un profilo"

#, elixir-format
#: lib/graphql/resolvers/actor.ex:105
msgid "Only moderators and administrators can unsuspend a profile"
msgstr "Solo i moderatori e gli amministratori possono riattivare un profilo"

#, elixir-format
#: lib/graphql/resolvers/actor.ex:29
msgid "Only remote profiles may be refreshed"
msgstr "È possibile aggiornare solo i profili remoti"

#, elixir-format
#: lib/graphql/resolvers/actor.ex:64
msgid "Profile already suspended"
msgstr "Profilo già sospeso"

#, elixir-format
#: lib/graphql/resolvers/participant.ex:96
msgid "A valid email is required by your instance"
msgstr "Un'email valida è richiesta dalla vostra istanza"

#, elixir-format
#: lib/graphql/resolvers/participant.ex:90
#: lib/graphql/resolvers/participant.ex:143
msgid "Anonymous participation is not enabled"
msgstr "La partecipazione anonima non è abilitata"

#, elixir-format
#: lib/graphql/resolvers/person.ex:210
msgid "Cannot remove the last administrator of a group"
msgstr "Impossibile rimuovere l'ultimo amministratore di un gruppo"

#, elixir-format
#: lib/graphql/resolvers/person.ex:207
msgid "Cannot remove the last identity of a user"
msgstr "Impossibile rimuovere l'ultima identità di un utente"

#, elixir-format
#: lib/graphql/resolvers/comment.ex:126
msgid "Comment is already deleted"
msgstr "Commento già cancellato"

#, elixir-format
#: lib/graphql/error.ex:101 lib/graphql/resolvers/discussion.ex:75
msgid "Discussion not found"
msgstr "Discussione non trovata"

#, elixir-format
#: lib/graphql/resolvers/report.ex:63 lib/graphql/resolvers/report.ex:82
msgid "Error while saving report"
msgstr "Errore nel salvare la segnalazione"

#, elixir-format
#: lib/graphql/resolvers/report.ex:102
msgid "Error while updating report"
msgstr "Errore durante l'aggiornamento del rapporto"

#, elixir-format
#: lib/graphql/resolvers/participant.ex:131
msgid "Event id not found"
msgstr "ID evento non trovato"

#, elixir-format
#: lib/graphql/error.ex:98 lib/graphql/resolvers/event.ex:355
#: lib/graphql/resolvers/event.ex:407
msgid "Event not found"
msgstr "Evento non trovato"

#, elixir-format
#: lib/graphql/resolvers/participant.ex:87
#: lib/graphql/resolvers/participant.ex:128 lib/graphql/resolvers/participant.ex:155
#: lib/graphql/resolvers/participant.ex:336
msgid "Event with this ID %{id} doesn't exist"
msgstr "L'evento con questo ID %{id} non esiste"

#, elixir-format
#: lib/graphql/resolvers/participant.ex:103
msgid "Internal Error"
msgstr "Errore Interno"

#, elixir-format
#: lib/graphql/resolvers/discussion.ex:225
msgid "No discussion with ID %{id}"
msgstr "Nessuna discussione con l'ID %{id}"

#, elixir-format
#: lib/graphql/resolvers/todos.ex:80 lib/graphql/resolvers/todos.ex:107
#: lib/graphql/resolvers/todos.ex:179 lib/graphql/resolvers/todos.ex:208 lib/graphql/resolvers/todos.ex:237
msgid "No profile found for user"
msgstr "Nessuno profilo trovato per l'utente"

#, elixir-format
#: lib/graphql/resolvers/feed_token.ex:64
msgid "No such feed token"
msgstr "Nessun token di rifornimento corrispondente"

#, elixir-format
#: lib/graphql/resolvers/participant.ex:259
msgid "Participant already has role %{role}"
msgstr "Il partecipante ha già il ruolo %{role}"

#, elixir-format
#: lib/graphql/resolvers/participant.ex:187
#: lib/graphql/resolvers/participant.ex:220 lib/graphql/resolvers/participant.ex:263
msgid "Participant not found"
msgstr "Partecipante non trovato"

#, elixir-format
#: lib/graphql/resolvers/person.ex:32
msgid "Person with ID %{id} not found"
msgstr "La persona con l'ID %{id} non è stata trovata"

#, elixir-format
#: lib/graphql/resolvers/person.ex:56
msgid "Person with username %{username} not found"
msgstr "La persona con il nome utente %{username} non è stata trovata"

#, elixir-format
#: lib/graphql/resolvers/post.ex:169 lib/graphql/resolvers/post.ex:203
msgid "Post ID is not a valid ID"
msgstr "L'ID del post non è un ID valido"

#, elixir-format
#: lib/graphql/resolvers/post.ex:172 lib/graphql/resolvers/post.ex:206
msgid "Post doesn't exist"
msgstr "Il post non esiste"

#, elixir-format
#: lib/graphql/resolvers/member.ex:82
msgid "Profile invited doesn't exist"
msgstr "Il profilo invitato non esiste"

#, elixir-format
#: lib/graphql/resolvers/member.ex:91 lib/graphql/resolvers/member.ex:95
msgid "Profile is already a member of this group"
msgstr "Il profilo è già un membro diquesto gruppo"

#, elixir-format
#: lib/graphql/resolvers/post.ex:133 lib/graphql/resolvers/post.ex:175
#: lib/graphql/resolvers/post.ex:209 lib/graphql/resolvers/resource.ex:90 lib/graphql/resolvers/resource.ex:132
#: lib/graphql/resolvers/resource.ex:165 lib/graphql/resolvers/resource.ex:199 lib/graphql/resolvers/todos.ex:58
#: lib/graphql/resolvers/todos.ex:83 lib/graphql/resolvers/todos.ex:110 lib/graphql/resolvers/todos.ex:182
#: lib/graphql/resolvers/todos.ex:214 lib/graphql/resolvers/todos.ex:246
msgid "Profile is not member of group"
msgstr "Il profilo non è membro del gruppo"

#, elixir-format
#: lib/graphql/resolvers/actor.ex:67 lib/graphql/resolvers/person.ex:233
msgid "Profile not found"
msgstr "Profilo non trovato"

#, elixir-format
#: lib/graphql/resolvers/report.ex:40
msgid "Report not found"
msgstr "Segnalazione non trovata"

#, elixir-format
#: lib/graphql/resolvers/resource.ex:169 lib/graphql/resolvers/resource.ex:196
msgid "Resource doesn't exist"
msgstr "La risorsa non esiste"

#, elixir-format
#: lib/graphql/resolvers/participant.ex:124
msgid "The event has already reached its maximum capacity"
msgstr "L'evento ha già raggiunto la sua massima capacità"

#, elixir-format
#: lib/graphql/resolvers/participant.ex:282
msgid "This token is invalid"
msgstr "Questo token non è valido"

#, elixir-format
#: lib/graphql/resolvers/todos.ex:176 lib/graphql/resolvers/todos.ex:243
msgid "Todo doesn't exist"
msgstr "L'elemento to-do non esiste"

#, elixir-format
#: lib/graphql/resolvers/todos.ex:77 lib/graphql/resolvers/todos.ex:211
#: lib/graphql/resolvers/todos.ex:240
msgid "Todo list doesn't exist"
msgstr "la lista non esiste"

#, elixir-format
#: lib/graphql/resolvers/feed_token.ex:73
msgid "Token does not exist"
msgstr "Il token non esiste"

#, elixir-format
#: lib/graphql/resolvers/feed_token.ex:67 lib/graphql/resolvers/feed_token.ex:70
msgid "Token is not a valid UUID"
msgstr "Il token non è un UUID valido"

#, elixir-format
#: lib/graphql/error.ex:96 lib/graphql/resolvers/person.ex:415
msgid "User not found"
msgstr "Utente non trovato"

#, elixir-format
#: lib/graphql/resolvers/person.ex:310
msgid "You already have a profile for this user"
msgstr "Hai già un profilo per questo utente"

#, elixir-format
#: lib/graphql/resolvers/participant.ex:134
msgid "You are already a participant of this event"
msgstr "Se già un partecipante di questo evento"

#, elixir-format
#: lib/graphql/resolvers/member.ex:85
msgid "You are not a member of this group"
msgstr "Non sei un membro di questo gruppo"

#, elixir-format
#: lib/graphql/resolvers/member.ex:155
msgid "You are not a moderator or admin for this group"
msgstr "Non sei un moderatore o amministratore di questo gruppo"

#, elixir-format
#: lib/graphql/resolvers/comment.ex:59
msgid "You are not allowed to create a comment if not connected"
msgstr "Non è consentito creare un commento se non si è collegati"

#, elixir-format
#: lib/graphql/resolvers/feed_token.ex:41
msgid "You are not allowed to create a feed token if not connected"
msgstr "Non puoi creare un token di rifornimento senza connessione"

#, elixir-format
#: lib/graphql/resolvers/comment.ex:134
msgid "You are not allowed to delete a comment if not connected"
msgstr "Non è consentito eliminare un commento se non si è collegati"

#, elixir-format
#: lib/graphql/resolvers/feed_token.ex:82
msgid "You are not allowed to delete a feed token if not connected"
msgstr "Non puoi eliminare un token di rifornimento senza connettersi"

#, elixir-format
#: lib/graphql/resolvers/comment.ex:93
msgid "You are not allowed to update a comment if not connected"
msgstr "Non è consentito aggiornare un commento se non si è collegati"

#, elixir-format
#: lib/graphql/resolvers/participant.ex:181
#: lib/graphql/resolvers/participant.ex:214
msgid "You can't leave event because you're the only event creator participant"
msgstr ""
"Non puoi lasciare l'evento perchè sei l'unico partecipante creatore di eventi"

#, elixir-format
#: lib/graphql/resolvers/member.ex:159
msgid "You can't set yourself to a lower member role for this group because you are the only administrator"
msgstr ""
"Non puoi impostare te stesso per un ruolo di membro inferiore per questo "
"gruppo perché sei l'unico amministratore"

#, elixir-format
#: lib/graphql/resolvers/comment.ex:122
msgid "You cannot delete this comment"
msgstr "Non puoi eliminare questo commento"

#, elixir-format
#: lib/graphql/resolvers/event.ex:403
msgid "You cannot delete this event"
msgstr "Non puoi eliminare questo evento"

#, elixir-format
#: lib/graphql/resolvers/member.ex:88
msgid "You cannot invite to this group"
msgstr "Non puoi invitare in questo gruppo"

#, elixir-format
#: lib/graphql/resolvers/feed_token.ex:76
msgid "You don't have permission to delete this token"
msgstr "Non hai il permesso di cancellare questo token"

#, elixir-format
#: lib/graphql/resolvers/admin.ex:54
msgid "You need to be logged-in and a moderator to list action logs"
msgstr "Devi essere connesso e un moderatore per elencare i log delle azioni"

#, elixir-format
#: lib/graphql/resolvers/report.ex:28
msgid "You need to be logged-in and a moderator to list reports"
msgstr "Devi essere connesso e un moderatore per elencare i rapporti"

#, elixir-format
#: lib/graphql/resolvers/report.ex:107
msgid "You need to be logged-in and a moderator to update a report"
msgstr "Devi essere connesso e un moderatore per aggiornare un rapporto"

#, elixir-format
#: lib/graphql/resolvers/report.ex:45
msgid "You need to be logged-in and a moderator to view a report"
msgstr "Devi essere connesso e un moderatore per visualizzare un rapporto"

#, elixir-format
#: lib/graphql/resolvers/admin.ex:246
msgid "You need to be logged-in and an administrator to access admin settings"
msgstr ""
"Devi essere connesso e un moderatore per accedere alle opzioni "
"dell'amministratore"

#, elixir-format
#: lib/graphql/resolvers/admin.ex:230
msgid "You need to be logged-in and an administrator to access dashboard statistics"
msgstr ""
"Devi essere connesso e un moderatore per accedere alle statistiche del "
"dashboard"

#, elixir-format
#: lib/graphql/resolvers/admin.ex:272
msgid "You need to be logged-in and an administrator to save admin settings"
msgstr ""
"Devi essere connesso e un moderatore per salvare le impostazioni "
"dell'amministratore"

#, elixir-format
#: lib/graphql/resolvers/discussion.ex:90
msgid "You need to be logged-in to access discussions"
msgstr "Devi essere connesso per accedere alle discussioni"

#, elixir-format
#: lib/graphql/resolvers/resource.ex:96
msgid "You need to be logged-in to access resources"
msgstr "Devi essere connesso per accedere alle risorse"

#, elixir-format
#: lib/graphql/resolvers/event.ex:313
msgid "You need to be logged-in to create events"
msgstr "Devi essere connesso per creare eventi"

#, elixir-format
#: lib/graphql/resolvers/post.ex:141
msgid "You need to be logged-in to create posts"
msgstr "Devi essere connesso per creare dei post"

#, elixir-format
#: lib/graphql/resolvers/report.ex:79
msgid "You need to be logged-in to create reports"
msgstr "Devi essere connesso per creare rapporti"

#, elixir-format
#: lib/graphql/resolvers/resource.ex:137
msgid "You need to be logged-in to create resources"
msgstr "Devi essere connesso per creare risorse"

#, elixir-format
#: lib/graphql/resolvers/event.ex:412
msgid "You need to be logged-in to delete an event"
msgstr "Devi essere connesso per eliminare un evento"

#, elixir-format
#: lib/graphql/resolvers/post.ex:214
msgid "You need to be logged-in to delete posts"
msgstr "Devi essere connesso per eliminare dei post"

#, elixir-format
#: lib/graphql/resolvers/resource.ex:204
msgid "You need to be logged-in to delete resources"
msgstr "Devi essere connesso per eliminare risorse"

#, elixir-format
#: lib/graphql/resolvers/participant.ex:108
msgid "You need to be logged-in to join an event"
msgstr "Devi essere connesso per partecipare a un evento"

#, elixir-format
#: lib/graphql/resolvers/participant.ex:225
msgid "You need to be logged-in to leave an event"
msgstr "Devi essere connesso per lasciare un evento"

#, elixir-format
#: lib/graphql/resolvers/event.ex:369
msgid "You need to be logged-in to update an event"
msgstr "Devi essere connesso per aggiornare un evento"

#, elixir-format
#: lib/graphql/resolvers/post.ex:180
msgid "You need to be logged-in to update posts"
msgstr "Devi essere connesso per aggiornare dei post"

#, elixir-format
#: lib/graphql/resolvers/resource.ex:174
msgid "You need to be logged-in to update resources"
msgstr "Devi essere connesso per aggiornare le risorse"

#, elixir-format
#: lib/graphql/resolvers/resource.ex:233
msgid "You need to be logged-in to view a resource preview"
msgstr "Devi essere connesso per visualizzare l'anteprima di una risorsa"

#, elixir-format
#: lib/graphql/resolvers/resource.ex:129
msgid "Parent resource doesn't belong to this group"
msgstr "La risorsa principale non appartiene a questo gruppo"

#, elixir-format
#: lib/mobilizon/users/user.ex:114
msgid "The chosen password is too short."
msgstr "La password scelta è troppo corta."

#, elixir-format
#: lib/mobilizon/users/user.ex:142
msgid "The registration token is already in use, this looks like an issue on our side."
msgstr ""
"Il token di registrazione è già in uso, questo sembra un problema dalla "
"nostra parte."

#, elixir-format
#: lib/mobilizon/users/user.ex:108
msgid "This email is already used."
msgstr "Questa email è già in uso."

#, elixir-format
#: lib/graphql/error.ex:97
msgid "Post not found"
msgstr "Post non trovato"

#, elixir-format
#: lib/graphql/error.ex:84
msgid "Invalid arguments passed"
msgstr "Sono stati trasmessi argomenti non validi"

#, elixir-format
#: lib/graphql/error.ex:90
msgid "Invalid credentials"
msgstr "Credenziali non valide"

#, elixir-format
#: lib/graphql/error.ex:88
msgid "Reset your password to login"
msgstr "Reimposta la tua password per connetterti"

#, elixir-format
#: lib/graphql/error.ex:95 lib/graphql/error.ex:100
msgid "Resource not found"
msgstr "Segnalazione non trovata"

#, elixir-format
#: lib/graphql/error.ex:102
msgid "Something went wrong"
msgstr "Qualcosa è andato storto"

#, elixir-format
#: lib/graphql/error.ex:83
msgid "Unknown Resource"
msgstr "Risorsa sconosciuta"

#, elixir-format
#: lib/graphql/error.ex:93
msgid "You don't have permission to do this"
msgstr "Non hai il permesso di farlo"

#, elixir-format
#: lib/graphql/error.ex:85
msgid "You need to be logged in"
msgstr "Devi essere connesso"

#, elixir-format
#: lib/graphql/resolvers/member.ex:116
msgid "You can't accept this invitation with this profile."
msgstr "Non puoi accettare l'invito con questo profilo."

#, elixir-format
#: lib/graphql/resolvers/member.ex:137
msgid "You can't reject this invitation with this profile."
msgstr "Non puoi rifiutare l'invito con questo profilo."

#, elixir-format
#: lib/graphql/resolvers/media.ex:71
msgid "File doesn't have an allowed MIME type."
msgstr "Il file non ha un tipo MIME consentito."

#, elixir-format
#: lib/graphql/resolvers/group.ex:213
msgid "Profile is not administrator for the group"
msgstr "Il profilo non è amministratore del gruppo"

#, elixir-format
#: lib/graphql/resolvers/event.ex:358
msgid "You can't edit this event."
msgstr "Non puoi modificare questo evento."

#, elixir-format
#: lib/graphql/resolvers/event.ex:361
msgid "You can't attribute this event to this profile."
msgstr "Non puo iattribuire questo evento a questo profilo."

#, elixir-format
#: lib/graphql/resolvers/member.ex:140
msgid "This invitation doesn't exist."
msgstr "Questo invito non esiste."

#, elixir-format
#: lib/graphql/resolvers/member.ex:185
msgid "This member already has been rejected."
msgstr "Questo memebro è già stato rifiutato."

#, elixir-format
#: lib/graphql/resolvers/member.ex:192
msgid "You don't have the right to remove this member."
msgstr "Non hai il diritto di rimuovere questo membro."

#, elixir-format
#: lib/mobilizon/actors/actor.ex:349
msgid "This username is already taken."
msgstr "Questo nome utente è già in uso."

#, elixir-format
#: lib/graphql/resolvers/discussion.ex:87
msgid "You must provide either an ID or a slug to access a discussion"
msgstr ""
"Devi fornire un ID o la stringa utente (ad es. <em>utente@mobilizon.sm</em>) "
"per accedere ad una discussione"

#, elixir-format
#: lib/graphql/resolvers/event.ex:308
msgid "Organizer profile is not owned by the user"
msgstr "Il profilo dell'organizzatore non è di proprietà dell'utente"

#, elixir-format
#: lib/graphql/resolvers/participant.ex:93
msgid "Profile ID provided is not the anonymous profile one"
msgstr "L'ID profilo fornito non è quello del profilo anonimo"

#, elixir-format
#: lib/graphql/resolvers/group.ex:159 lib/graphql/resolvers/group.ex:201
#: lib/graphql/resolvers/person.ex:148 lib/graphql/resolvers/person.ex:182 lib/graphql/resolvers/person.ex:304
msgid "The provided picture is too heavy"
msgstr "L'immagine inserita è troppo pesante"

#, elixir-format
#: lib/web/views/utils.ex:34
msgid "Index file not found. You need to recompile the front-end."
msgstr "Il file di indice non è stato trovato. Devi ricompilare il front-end."

#, elixir-format
#: lib/graphql/resolvers/resource.ex:126
msgid "Error while creating resource"
msgstr "Errore durante la creazione della risorsa"

#, elixir-format
#: lib/graphql/resolvers/user.ex:483
msgid "Invalid activation token"
msgstr "Token di attivazione non valido"

#, elixir-format
#: lib/graphql/resolvers/resource.ex:223
msgid "Unable to fetch resource details from this URL."
msgstr "Impossibile recuperare i dettagli della risorsa da questa URL."

#, elixir-format
#: lib/graphql/resolvers/event.ex:173 lib/graphql/resolvers/participant.ex:253
#: lib/graphql/resolvers/participant.ex:328
msgid "Provided profile doesn't have moderator permissions on this event"
msgstr ""
"Il profilo del moderatore fornito non dispone dell'autorizzazione per questo "
"evento"

#, elixir-format
#: lib/graphql/resolvers/event.ex:294
msgid "Organizer profile doesn't have permission to create an event on behalf of this group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/event.ex:349
msgid "This profile doesn't have permission to update an event on behalf of this group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:163
msgid "Your e-mail has been denied registration or uses a disallowed e-mail provider"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/comment.ex:129
msgid "Comment not found"
msgstr "Evento non trovato"

#, elixir-format
#: lib/graphql/resolvers/discussion.ex:129
msgid "Error while creating a discussion"
msgstr "Errore durante la creazione della risorsa"

#, elixir-format
#: lib/graphql/resolvers/user.ex:606
msgid "Error while updating locale"
msgstr "Errore durante l'aggiornamento del rapporto"

#, elixir-format
#: lib/graphql/resolvers/person.ex:307
msgid "Error while uploading pictures"
msgstr "Errore durante l'aggiornamento del rapporto"

#, elixir-format
#: lib/graphql/resolvers/participant.ex:190
msgid "Failed to leave the event"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:209
msgid "Failed to update the group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:447
msgid "Failed to update user email"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:479
msgid "Failed to validate user email"
msgstr "Impossibile convalidare l'utente"

#, elixir-format
#: lib/graphql/resolvers/participant.ex:146
msgid "The anonymous actor ID is invalid"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/resource.ex:162
msgid "Unknown error while updating resource"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/comment.ex:84
msgid "You are not the comment creator"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:404
msgid "You cannot change your password."
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:321
msgid "Format not supported"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:305
msgid "A dependency needed to export to %{format} is not installed"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:313
msgid "An error occured while saving export"
msgstr ""

#, elixir-format
#: lib/web/controllers/export_controller.ex:30
msgid "Export to format %{format} is not enabled on this instance"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:165
msgid "Only admins can create groups"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/event.ex:301
msgid "Only groups can create events"
msgstr ""

#, elixir-format, fuzzy
#: lib/graphql/resolvers/event.ex:287
msgid "Unknown error while creating event"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:460
msgid "User cannot change email"
msgstr ""
